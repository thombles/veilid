# Veilid Bindings for Python

## Usage

To use:
```
poetry add veilid_python
```
or 
```
pip3 install veilid_python
```


## Development

To run tests:
```
poetry run pytest
```

To update schema for validation with the latest copy from a running `veilid-server`:
```
./update_schema.sh
```

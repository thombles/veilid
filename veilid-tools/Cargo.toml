[package]
name = "veilid-tools"
version = "0.1.7"
authors = ["Veilid Team <contact@veilid.com>"]
license = "MPL-2.0"
edition = "2021"

[lib]
# staticlib for iOS tests, cydlib for android tests, rlib for everything else
crate-type = [ "cdylib", "staticlib", "rlib" ]

[features]
default = []
rt-async-std = [ "async-std", "async_executors/async_std" ]
rt-tokio = [ "tokio", "tokio-util", "async_executors/tokio_tp", "async_executors/tokio_io", "async_executors/tokio_timer" ]
rt-wasm-bindgen = [ "async_executors/bindgen", "async_executors/timer"]

veilid_tools_android_tests = [ "dep:paranoid-android" ]
veilid_tools_ios_tests = [ "dep:oslog", "dep:tracing-oslog" ]
tracing = [ "dep:tracing", "dep:tracing-subscriber" ]
network-result-extra = []
network-result-info = []

[dependencies]
tracing = { version = "^0", features = ["log", "attributes"], optional = true }
tracing-subscriber = { version = "^0", optional = true }
log = { version = "^0" }
eyre = "^0"
static_assertions = "^1"
cfg-if = "^1"
thiserror = "^1"
futures-util = { version = "^0", default_features = false, features = ["alloc"] }
parking_lot = "^0"
once_cell = "^1"
stop-token = { version = "^0", default-features = false }
rand = "^0.7"
rust-fsm = "^0"
backtrace = "^0"
fn_name = "^0"
range-set-blaze = "0.1.5"
flume = { version = "^0", features = ["async"] }

# Dependencies for native builds only
# Linux, Windows, Mac, iOS, Android
[target.'cfg(not(target_arch = "wasm32"))'.dependencies]
async-std = { version = "^1", features = ["unstable"], optional = true}
tokio = { version = "^1", features = ["full"], optional = true}
tokio-util = { version = "^0", features = ["compat"], optional = true}
futures-util = { version = "^0", default-features = false, features = ["async-await", "sink", "std", "io"] }
chrono = "^0"

libc = "^0"
nix = "^0"

# Dependencies for WASM builds only
[target.'cfg(target_arch = "wasm32")'.dependencies]
wasm-bindgen = "^0"
js-sys = "^0"
wasm-bindgen-futures = "^0"
async_executors = { version = "^0", default-features = false}
async-lock = "^2"
send_wrapper = { version = "^0.6", features = ["futures"] }

# Dependencies for Android
[target.'cfg(target_os = "android")'.dependencies]
jni = "^0"
jni-sys = "^0"
ndk = { version = "^0.7" }
ndk-glue = { version = "^0.7", features = ["logger"] }
lazy_static = "^1.4.0"
paranoid-android = { version = "^0", optional = true }
android-logd-logger = "0.2.1"

# Dependencies for Windows
# [target.'cfg(target_os = "windows")'.dependencies]
# windows = { version = "^0", features = [ "Win32_NetworkManagement_Dns", "Win32_Foundation", "alloc" ]}
# windows-permissions = "^0"

# Dependencies for iOS
[target.'cfg(target_os = "ios")'.dependencies]
oslog = { version = "^0", optional = true }
tracing-oslog = { version = "^0", optional = true }

### DEV DEPENDENCIES

[dev-dependencies]
serial_test = "^0"
simplelog = { version = "^0.12", features = [ "test" ] }

[target.'cfg(target_arch = "wasm32")'.dev-dependencies]
console_error_panic_hook = "^0"
wasm-bindgen-test = "^0"
wee_alloc = "^0"
wasm-logger = "^0"
tracing-wasm = { version = "^0" }
parking_lot = { version = "^0", features = ["wasm-bindgen"]}

### BUILD OPTIONS

[package.metadata.wasm-pack.profile.release]
wasm-opt = ["-O", "--enable-mutable-globals"]

[package.metadata.ios]
build_targets = ["aarch64-apple-ios", "aarch64-apple-ios-sim", "x86_64-apple-ios"]
deployment_target = "12.0"
build_id_prefix = "com.veilid.veilidtools"
